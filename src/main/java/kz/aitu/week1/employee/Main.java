package kz.aitu.week1.employee;

public class Main {
    public static void main(String[] args) {

        kz.aitu.week1.employee.Employee employee1 = new kz.aitu.week1.employee.Employee("Nurzhan", 20, 50);
        kz.aitu.week1.employee.Employee employee2 = new kz.aitu.week1.employee.Employee("Adilet", 30, 40);
        kz.aitu.week1.employee.Employee employee3  = new kz.aitu.week1.employee.Employee("Serik", 40, 60);


        employee2.changeRate(80);
        System.out.println(employee2);

        System.out.println(Employee.totalSum + " Employee: ");
        System.out.println("Nurzhan");
        System.out.println("Adilet");
        System.out.println("Serik");

        System.out.println("Salary:");
        System.out.println("Serik: " + employee3.Salary());
        System.out.println("Nurzhan: " + employee1.Salary());
        System.out.println("Adilet: " + employee2.Salary());

        System.out.println(Employee.totalHours + " Hours ");

        System.out.println("Bonuses: ");
        System.out.println("Serik: " + employee3.Bonus());

    }
}
