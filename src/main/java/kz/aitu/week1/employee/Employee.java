package kz.aitu.week1.employee;

public class Employee {
    private String name;
    private int rating;
    private int hours;
    static int totalSum=0;
    static int totalHours=0;


    public Employee(String name, int rating) {
        this.name = name;
        this.rating = rating;
    }


    public Employee(String name, int rating, int hours) {
        this.name = name;
        this.rating = rating;
        this.hours=hours;
        totalSum++;
        totalHours+=hours;

    }

    public String getName() { return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rate) {
        this.rating = rating;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {

        this.hours = hours;
    }


    public int Salary() {
        int salary = getRating() * getHours();
        return salary;
    }

    @Override
    public String toString() {
        return "Change the rate  " + name + ", rate=" + rating + ", hours=" + hours + "";
    }

    public void changeRate(int changeRate) {
        this.rating = changeRate;
    }

    public int Bonus() {
        int percent = 10;
        int bonuses = Salary() * (100 + percent) / 100;
        return bonuses;
    }
}
