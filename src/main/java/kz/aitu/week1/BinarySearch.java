package kz.aitu.week1;

public class BinarySearch {
    public static void BinarySearch(int arr[], int first, int last, int key){
        int mid = (first + last)/2;
        while( first <= last ){
            if ( arr[mid] < key ){
                first = mid + 1;
            }else if ( arr[mid] == key ){
                System.out.println("Index: " + mid);
                break;
            }else{
                last = mid - 1;
            }
            mid = (first + last)/2;
        }
        if ( first > last ){
            System.out.println("Element is not found!");
        }
    }
    public static void main(String args[]){
        int arr[] = {3,8,1,7,11};
        int key = 7;
        int last=arr.length-1;
        BinarySearch(arr,0,last,key);
    }
}
