package kz.aitu.week1;

public class FindMin {

        public int min(int [] array) {
            int min = array[0];

            for(int i=0; i<array.length; i++ ) {
                if(array[i]<min) {
                    min = array[i];
                }
            }
            return min;
        }

        public static void main(String args[]) {
            int[] myArray = {1, 5, 11, 31,25 };
            FindMin m = new FindMin();
            System.out.println("Minimum value in the array is::"+m.min(myArray));
        }
    }

