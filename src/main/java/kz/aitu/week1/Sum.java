package kz.aitu.week1;

public class Sum {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int[] arr = {1 , 2 ,3 ,4 ,5 ,6 ,7 ,8 ,9};

        System.out.println("Enter your number: ");
        int n = in.nextInt();

        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] + arr[j] == n) {
                    System.out.println(arr[i] + " + " +  arr[j] + " = " + n);
                }
            }
        }

    }

}

